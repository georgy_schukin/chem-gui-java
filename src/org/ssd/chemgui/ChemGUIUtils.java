package org.ssd.chemgui;

import java.io.File;

public class ChemGUIUtils {
    public static String getFileExtension(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf(".") > 0) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        }
        return "";
    }

    public static String toJmolHexString(java.awt.Color color) {
        return String.format("[x%06X]", (0xFFFFFF & color.getRGB())).toUpperCase();
    }

    public static String toJmolHexString(javafx.scene.paint.Color color) {
        return String.format( "[x%02X%02X%02X]",
                (int)(color.getRed() * 255),
                (int)(color.getGreen() * 255),
                (int)(color.getBlue() * 255)).toUpperCase();
    }

    public static String toWXYZFileName(File file) {
        String fileName = file.getName();
        String extension = getFileExtension(file);
        if (!extension.isEmpty()) {
            return fileName.replaceFirst("." + extension, ".wxyz");
        } else {
            return fileName + ".wxyz";
        }
    }
}
