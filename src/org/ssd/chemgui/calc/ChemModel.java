package org.ssd.chemgui.calc;

import org.ssd.chemgui.ChemGUIUtils;
import org.ssd.chemgui.util.TempDirectory;

import java.io.File;
import java.util.List;

public class ChemModel {
    private int currentIonIndex = 0;
    private double currentGridZize = 0.01;

    private ChemCalc calc = new ChemCalc();

    private TempDirectory tempDir = new TempDirectory("chemgui");

    public ChemModel() {
    }

    public ChemModel(File file) throws Exception {
        String extension = ChemGUIUtils.getFileExtension(file);
        switch (extension) {
            case "res":
            case "ins":
                throw new UnsupportedOperationException("This file type is not supported yet!");
            case "wxyz":
                initFromWXYZFile(file.getAbsolutePath());
                break;
            default:
                throw new UnsupportedOperationException("Unsupported file type");
        }
    }

    private void initFromWXYZFile(String filename) throws Exception {
        calc = new ChemCalc(filename);
        currentIonIndex = 0;
        currentGridZize = calc.getMinGridSize();
    }

    public void setCurrentIonIndex(int num) {
        currentIonIndex = num;
    }

    public int getCurrentIonIndex() {
        return currentIonIndex;
    }

    public double getCurrentGridZize() {
        return currentGridZize;
    }

    public void setCurrentGridZize(double size) {
        currentGridZize = size;
    }

    public int getNumOfIons() {
        return calc.getIonTypes().size();
    }

    public String getIonLabel(int num) {
        return calc.getIonTypes().get(num);
    }

    public double getMinGridSize() {
        return calc.getMinGridSize();
    }

    public boolean isReady() {
        return calc.isReady();
    }

    public List<String> calcCubFiles() throws Exception {
        return calc.calcCubFiles(currentIonIndex, currentGridZize, tempDir.getPath().toString());
    }
}
