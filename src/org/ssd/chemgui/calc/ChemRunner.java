package org.ssd.chemgui.calc;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ChemRunner {
    private static final String calcCommand = "bvscalc_c";
    private static final String parseCommand = "parse";

    public static String calc(String args) throws Exception {
        return run(calcCommand, args);
    }

    public static String parse(String args) throws Exception {
        return run(parseCommand, args);
    }

    private static String run(String command, String args) throws Exception {
        StringBuilder output = new StringBuilder();

        try {
            String commandToRun = command + " " + args;
            System.out.println(commandToRun);
            Process p = Runtime.getRuntime().exec(commandToRun);
            p.waitFor();

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line);
                output.append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        System.out.println(output.toString());
        return output.toString();
    }
}
