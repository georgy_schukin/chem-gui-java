package org.ssd.chemgui.calc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChemCalc {
    private String filename = "";
    private List<String> ionTypes = new ArrayList<>();
    private double minGridSize = 0.01;
    private boolean isReady = false;

    public ChemCalc() {
    }

    public ChemCalc(String filename) throws Exception {
        this.filename = filename;
        String result = ChemRunner.calc(filename + " info");
        String[] lines = result.split("\n");
        for (String s : lines) {
            if (s.isEmpty()) {
                break;
            }
            ionTypes.add(s);
        }
        minGridSize = Double.parseDouble(lines[lines.length - 1]);
        isReady = true;
    }

    public boolean isReady() {
        return isReady;
    }

    public List<String> getIonTypes() {
        return ionTypes;
    }

    public double getMinGridSize() {
        return minGridSize;
    }

    public List<String> calcCubFiles(int ionNumber, double gridSize, String outputDir) throws Exception {
        String result = ChemRunner.calc(this.filename + " calc " + ionNumber + " " + gridSize + " " + outputDir);
        return Arrays.asList(result.split("\n"));
    }
}
