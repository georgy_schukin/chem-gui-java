package org.ssd.chemgui.jmol;

import javafx.embed.swing.SwingNode;
import javafx.scene.paint.Color;
import org.ssd.chemgui.ChemGUIUtils;

import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyJmolScene {
    private MyJmolPanel jmolPanel;

    private List<String> cubFileTypes = Arrays.asList("BVS", "DA", "GII");
    private int currentCubFileTypeIndex = 0;
    private double currentIsoCutoff = 0.1;
    private Color currentIsoColor = Color.RED;
    private Color currentBGColor = Color.GREEN;
    private double currentIsoTranslucency = 0.0;

    private List<String> currentCubFiles = new ArrayList<>();

    public MyJmolScene() {
    }

    public SwingNode createNode() {
        SwingNode node = new SwingNode();
        initJmolPanel(node);
        return node;
    }

    public void openConsole() {
        getJmolPanel().openConsole();
    }

    public int getNumOfCubFileTypes() {
        return cubFileTypes.size();
    }

    public String getCubFileType(int index) {
        return cubFileTypes.get(index);
    }

    public int getCurrentCubFileTypeIndex() {
        return currentCubFileTypeIndex;
    }

    public void setCurrentCubFileTypeIndex(int index) {
        currentCubFileTypeIndex = index;
        generateIsosurface();
    }

    public double getCurrentIsoCutoff() {
        return currentIsoCutoff;
    }

    public void setCurrentIsoCutoff(double cutoff) {
        currentIsoCutoff = cutoff;
        updateIsosurface();
    }

    public Color getCurrentIsoColor() {
        return currentIsoColor;
    }

    public void setCurrentIsoColor(Color color) {
        currentIsoColor = color;
        updateIsosurfaceColor();
    }

    public Color getCurrentBGColor() {
        return currentBGColor;
    }

    public void setCurrentBGColor(Color color) {
        currentBGColor = color;
        updateBackgroundColor();
    }

    public double getCurrentIsoTranslucency() {
        return currentIsoTranslucency;
    }

    public void setCurrentIsoTranslucency(double translucency) {
        currentIsoTranslucency = translucency;
        updateIsosurfaceTranslucency();
    }

    public void setCurrentCubFiles(List<String> files) {
        currentCubFiles.clear();
        currentCubFiles.addAll(files);
        generateIsosurface();
    }

    private void generateIsosurface() {
        clearView();
        updateIsosurface();
        updateIsosurfaceColor();
        updateIsosurfaceTranslucency();
    }

    private void clearView() {
        script("select all");
        script("delete");
        script("isosurface delete");
        updateBackgroundColor();
    }

    private void updateIsosurface() {
        if (!hasIsosurface()) {
            return;
        }
        String cubFile = currentCubFiles.get(currentCubFileTypeIndex).replace("\\", "/");
        script("isosurface delete");
        script("isosurface IS0 cutoff " + currentIsoCutoff + " \"" + cubFile + "\" backlit");
    }

    private void updateBackgroundColor() {
        script("background " + ChemGUIUtils.toJmolHexString(currentBGColor));
    }

    private void updateIsosurfaceColor() {
        if (hasIsosurface()) {
            script("color isosurface " + ChemGUIUtils.toJmolHexString(currentIsoColor));
        }
    }

    private void updateIsosurfaceTranslucency() {
        if (hasIsosurface()) {
            script("isosurface translucent " + currentIsoTranslucency);
        }
    }

    private boolean hasIsosurface() {
        return !currentCubFiles.isEmpty();
    }

    private void script(String command) {
        getJmolPanel().script(command);
    }

    private void initJmolPanel(final SwingNode swingNode) {
        SwingUtilities.invokeLater(() -> {
            MyJmolPanel panel = getJmolPanel();
            panel.setPreferredSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
            clearView();
            swingNode.setContent(panel);
        });
    }

    private MyJmolPanel getJmolPanel() {
        synchronized (this) {
            if (jmolPanel == null) {
                jmolPanel = new MyJmolPanel();
            }
        }
        return jmolPanel;
    }
}
