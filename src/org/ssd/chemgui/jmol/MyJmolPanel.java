package org.ssd.chemgui.jmol;

import org.jmol.adapter.smarter.SmarterJmolAdapter;
import org.jmol.api.JmolAdapter;
import org.jmol.api.JmolViewer;

import javax.swing.*;
import java.awt.*;

public class MyJmolPanel extends JPanel implements MyJmolControl {
    private static final long serialVersionUID = -3661941083797644242L;

    private JmolViewer viewer;

    public MyJmolPanel() {
        JmolAdapter adapter = new SmarterJmolAdapter();
        viewer = JmolViewer.allocateViewer(this, adapter);
    }

    public JmolViewer getViewer() {
        return viewer;
    }

    public void loadFile(String path) {
        this.script("load " + path);
    }

    public void openConsole() {
        this.script("console");
    }

    @Override
    public void script(String command) {
        viewer.evalString(command);
    }

    private final Dimension currentSize = new Dimension();
    private final Rectangle rectClip = new Rectangle();

    public void paint(Graphics g) {
        getSize(currentSize);
        g.getClipBounds(rectClip);
        viewer.renderScreenImage(g, currentSize.width, currentSize.height);
    }
}
