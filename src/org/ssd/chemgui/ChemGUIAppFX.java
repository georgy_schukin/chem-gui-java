package org.ssd.chemgui;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.stage.FileChooser;
import org.ssd.chemgui.calc.ChemModel;
import org.ssd.chemgui.ctrl.EditableSpinner;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import org.ssd.chemgui.jmol.MyJmolScene;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

public class ChemGUIAppFX extends Application {

    private MyJmolScene scene = new MyJmolScene();

    private EventHandler<ActionEvent> openFileHandler;
    private EventHandler<ActionEvent> exitAppHandler;

    private FileChooser fileChooser;

    private TitledPane gridPane = new TitledPane();
    private Label infoLabel = new Label("");

    private ChemModel model = new ChemModel();

    @Override
    public void start(Stage primaryStage) {
        Node jmolPanelNode = scene.createNode();
        Node menu = createMenuBar(primaryStage);
        Node toolBar = createToolBar();
        Node statusBar = createStatusBar();

        BorderPane pane = new BorderPane();
        pane.setTop(menu);
        pane.setLeft(toolBar);
        pane.setCenter(jmolPanelNode);
        pane.setBottom(statusBar);
        Scene scene = new Scene(pane, 1024, 700);

        primaryStage.setOnCloseRequest((event) -> exitApp());
        primaryStage.setTitle("ChemGUI");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private MenuBar createMenuBar(final Stage stage) {
        MenuBar menubar = new MenuBar();
        menubar.getMenus().add(createFileMenu(stage));
        menubar.getMenus().add(createToolsMenu());
        menubar.getMenus().add(createHelpMenu());
        return menubar;
    }

    private Menu createFileMenu(final Stage stage) {
        Menu file = new Menu("_File");

        MenuItem openItem = new MenuItem("_Open...");
        openItem.setAccelerator(new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN));
        openItem.setOnAction(getOpenFileHandler(stage));

        MenuItem exitItem = new MenuItem("_Exit");
        exitItem.setAccelerator(new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN));
        exitItem.setOnAction(getExitAppHandler());

        file.getItems().addAll(openItem, new SeparatorMenuItem(), exitItem);

        return file;
    }

    private Menu createToolsMenu() {
        Menu tools = new Menu("_Tools");

        MenuItem consoleItem = new MenuItem("_Console...");
        consoleItem.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN));
        consoleItem.setOnAction((event) -> scene.openConsole());

        tools.getItems().addAll(consoleItem);

        return tools;
    }

    private Menu createHelpMenu() {
        Menu help = new Menu("_Help");

        MenuItem aboutItem = new MenuItem("_About...");
        aboutItem.setAccelerator(new KeyCodeCombination(KeyCode.A, KeyCombination.CONTROL_DOWN));
        aboutItem.setOnAction((event) -> {
                Alert aboutDlg = new Alert(Alert.AlertType.INFORMATION);
                aboutDlg.setTitle("About ChemGUI");
                aboutDlg.setHeaderText(null);
                aboutDlg.setContentText("ChemGUI v1.0\n" +
                        "GUI: Georgy Schukin\n" +
                        "Math: Vladislav Komarov\n" +
                        "Visualization: Jmol");
                aboutDlg.showAndWait();
        });

        help.getItems().addAll(aboutItem);

        return help;
    }

    private Node createToolBar() {
        TitledPane gridPane = createGridPane();
        TitledPane isoPane = createIsosurfacePane();

        VBox box = new VBox(gridPane, isoPane);
        ScrollPane scrollPane = new ScrollPane(box);

        HBox root = new HBox(scrollPane);

        return root;
    }

    private TitledPane createGridPane() {
        TitledPane gridPane = new TitledPane();
        gridPane.setText("Grid");
        gridPane.setExpanded(true);

        initGridPane(gridPane);

        this.gridPane = gridPane;
        return gridPane;
    }

    private void initGridPane(TitledPane pane) {
        ComboBox<String> ionTypes = new ComboBox<>();
        for (int i = 0; i < model.getNumOfIons(); i++) {
            ionTypes.getItems().add(model.getIonLabel(i));
        }
        ionTypes.setOnAction((event) -> {
            ComboBox cb = (ComboBox)event.getSource();
            model.setCurrentIonIndex(cb.getSelectionModel().getSelectedIndex());
        });
        if (ionTypes.getItems().size() > model.getCurrentIonIndex()) {
            ionTypes.getSelectionModel().select(model.getCurrentIonIndex());
        }

        EditableSpinner<Double> gridSize = new EditableSpinner<>(
                new SpinnerValueFactory.DoubleSpinnerValueFactory(model.getMinGridSize(), 100.0,
                model.getCurrentGridZize(), 0.1));
        gridSize.valueProperty().addListener((observable, oldValue, newValue) -> {
            model.setCurrentGridZize(newValue);
        });
        gridSize.getEditor().setPrefWidth(100.0);

        Button calcButton = new Button("Generate isosurfaces");
        calcButton.setOnAction((event) -> calculateBVS());
        if (!model.isReady()) {
            calcButton.setDisable(true);
        }

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(5));
        grid.setVgap(10);

        grid.add(new Label("Ion type: "), 0, 0);
        grid.add(ionTypes, 1, 0);
        grid.add(new Label("Grid size: "), 0, 1);
        grid.add(gridSize, 1, 1);
        grid.add(calcButton, 0, 2, 2, 1);

        pane.setContent(grid);
    }

    private TitledPane createIsosurfacePane() {
        TitledPane isoPane = new TitledPane();
        isoPane.setText("Isosurface");
        isoPane.setExpanded(true);

        initIsosurfacePane(isoPane);

        return isoPane;
    }

    private void initIsosurfacePane(TitledPane pane) {
        ComboBox<String> cubTypes = new ComboBox<>();
        for (int i = 0; i < scene.getNumOfCubFileTypes(); i++) {
            cubTypes.getItems().add(scene.getCubFileType(i));
        }
        cubTypes.setOnAction((event) -> {
            ComboBox cb = (ComboBox)event.getSource();
            scene.setCurrentCubFileTypeIndex(cb.getSelectionModel().getSelectedIndex());
        });
        cubTypes.getSelectionModel().select(scene.getCurrentCubFileTypeIndex());

        EditableSpinner<Double> isoCutoff = new EditableSpinner<>(
                new SpinnerValueFactory.DoubleSpinnerValueFactory(0.1, 100, scene.getCurrentIsoCutoff(), 0.1));
        isoCutoff.valueProperty().addListener((observable, oldValue, newValue) -> {
            scene.setCurrentIsoCutoff(newValue);
        });
        isoCutoff.getEditor().setPrefWidth(100.0);

        ColorPicker isoColor = new ColorPicker(scene.getCurrentIsoColor());
        isoColor.setOnAction((event) -> {
            ColorPicker cp = (ColorPicker)event.getSource();
            scene.setCurrentIsoColor(cp.getValue());
        });

        ColorPicker bgColor = new ColorPicker(scene.getCurrentBGColor());
        bgColor.setOnAction((event) -> {
            ColorPicker cp = (ColorPicker)event.getSource();
            scene.setCurrentBGColor(cp.getValue());
        });

        Slider isoTranslucency = new Slider(0.0, 1.0, scene.getCurrentIsoTranslucency());
        isoTranslucency.setBlockIncrement(0.1);
        isoTranslucency.valueProperty().addListener((observable, oldValue, newValue) -> {
            scene.setCurrentIsoTranslucency(newValue.doubleValue());
        });

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(5));
        grid.setVgap(10);

        grid.add(new Label("Cub file: "), 0, 0);
        grid.add(cubTypes, 1, 0);
        grid.add(new Label("Cutoff: "), 0, 1);
        grid.add(isoCutoff, 1, 1);
        grid.add(new Label("Color: "), 0, 2);
        grid.add(isoColor, 1, 2);
        grid.add(new Label("Translucency: "), 0, 3);
        grid.add(isoTranslucency, 1, 3);
        grid.add(new Label("Background: "), 0, 4);
        grid.add(bgColor, 1, 4);

        pane.setContent(grid);
    }

    private Node createStatusBar() {
        infoLabel = new Label("");
        HBox box = new HBox(5, infoLabel);
        box.setPadding(new Insets(2));
        return box;
    }

    private void setStatusMessage(String msg) {
        infoLabel.textProperty().unbind();
        infoLabel.textProperty().setValue(msg);
    }

    private void bindStatusMessageTo(ObservableValue<? extends String> value) {
        infoLabel.textProperty().bind(value);
    }

    private void calculateBVS() {
        if (!model.isReady()) {
            return;
        }
        Task<List<String>> task = new Task<List<String>>() {
            @Override
            protected List<String> call() throws Exception {
                updateMessage("Calculating...");
                return model.calcCubFiles();
            }
        };
        bindStatusMessageTo(task.messageProperty());
        task.setOnSucceeded((event) -> {
            setStatusMessage("Done");
            scene.setCurrentCubFiles(task.getValue());
        });
        task.setOnFailed((event) -> {
            setStatusMessage("Failed");
            showError(task.getException().getMessage());
        });
        new Thread(task).start();
    }

    private EventHandler<ActionEvent> getOpenFileHandler(final Stage stage) {
        if (openFileHandler == null) {
            openFileHandler = (event) -> {
                FileChooser fc = getFileChooser();
                File file = fc.showOpenDialog(stage);
                if (file != null) {
                    openFile(file, stage);
                    fc.setInitialDirectory(file.getParentFile());
                }
            };
        }
        return openFileHandler;
    }

    private void openFile(File file, final Stage stage) {
        Task<ChemModel> task = new Task<ChemModel>() {
            @Override
            protected ChemModel call() throws Exception {
                return new ChemModel(file);
            }
        };
        task.setOnSucceeded((event) -> {
            this.model = task.getValue();
            initGridPane(this.gridPane);
            stage.setTitle("ChemGUI: " + file.getAbsolutePath());
        });
        task.setOnFailed((event) -> showError(task.getException().getMessage()));
        new Thread(task).start();
    }

    private void showError(String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error!");
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }

    private EventHandler<ActionEvent> getExitAppHandler() {
        if (exitAppHandler == null) {
            exitAppHandler = (event) -> exitApp();
        }
        return exitAppHandler;
    }

    private void exitApp() {
        Platform.exit();
        System.exit(0);
    }

    private FileChooser getFileChooser() {
        if (fileChooser == null) {
            fileChooser = new FileChooser();
            fileChooser.setTitle("Open file");
            FileChooser.ExtensionFilter shxFiles = new FileChooser.ExtensionFilter("ShelX files", "*.res", "*.ins");
            FileChooser.ExtensionFilter wxyzFiles = new FileChooser.ExtensionFilter("WXYZ files", "*.wxyz");
            FileChooser.ExtensionFilter allFiles = new FileChooser.ExtensionFilter("All files", "*.*");
            fileChooser.getExtensionFilters().addAll(shxFiles, wxyzFiles, allFiles);
            fileChooser.setSelectedExtensionFilter(shxFiles);
            fileChooser.setInitialDirectory(Paths.get("").toAbsolutePath().toFile());
        }
        return fileChooser;
    }

    public static void main(String[] args) {
        launch(args);
    }
}