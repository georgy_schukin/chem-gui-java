package org.ssd.chemgui;

import org.ssd.chemgui.calc.ChemCalc;
import org.ssd.chemgui.calc.ChemRunner;
import org.ssd.chemgui.jmol.MyJmolPanel;
import org.ssd.chemgui.util.TempDirectory;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ChemGUIApp extends JFrame {

    //private ChemGUIWrap wrapper = getWrapper();

    private ChemCalc calc = new ChemCalc();

    private JPanel toolbarPanel;

    private MyJmolPanel jmolPanel;

    private JToolBar fileToolBar;
    private JToolBar isosurfaceToolBar;

    private int currentIonIndex = 0;
    private double currentGridSize = 0.1;

    private Color currentIsoColor = new Color(1.0f, 0.0f, 0.0f);
    private Color currentBGColor = new Color(0.0f, 1.0f, 0.0f);
    private double currentIsoCutoff = 1.0;
    private double currentIsoTranslucency = 0.0;

    private List<String> cubFiles = new ArrayList<>();
    private int currentCubFileIndex = 0;

    private JLabel statusInfo;

    private ActionListener openFileListener;
    private ActionListener exitListener;

    private TempDirectory tempDir;

    private JFileChooser fileChooser;

    public ChemGUIApp() {
        tempDir = new TempDirectory("chemgui");
        tempDir.deleteOnExit();
        fileChooser = makeFileChooser();
        initUI();
        clearView();
    }

    private void initUI() {
        initActionListeners();
        initJmolPanel();
        initMenuBar();
        initToolBars();
        initStatusBar();

        pack();

        setTitle("ChemGUI");
        setSize(1024, 768);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initActionListeners() {
        openFileListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                int returnVal = fileChooser.showOpenDialog(ChemGUIApp.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    fileChooser.setCurrentDirectory(selectedFile.getParentFile());
                    openFile(selectedFile);
                }
            }
        };

        exitListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }
        };
    }

    private JFileChooser makeFileChooser() {
        JFileChooser fc = new JFileChooser();
        fc.addChoosableFileFilter(new FileNameExtensionFilter("ShelX Files", "res", "ins"));
        fc.addChoosableFileFilter(new FileNameExtensionFilter("WYZ Files", "wxyz"));
        return fc;
    }

    private void initMenuBar() {
        JMenuBar menubar = new JMenuBar();

        menubar.add(createFileMenu());
        menubar.add(createToolsMenu());
        menubar.add(createHelpMenu());

        setJMenuBar(menubar);
    }

    private JMenu createFileMenu() {
        JMenu file = new JMenu("File");
        file.setMnemonic(KeyEvent.VK_F);

        JMenuItem openItem = new JMenuItem("Open...", KeyEvent.VK_O);
        openItem.setAccelerator(KeyStroke.getKeyStroke('O', InputEvent.CTRL_DOWN_MASK));
        openItem.setToolTipText("Open file");
        openItem.addActionListener(openFileListener);

        JMenuItem exitItem = new JMenuItem("Exit", KeyEvent.VK_E);
        exitItem.setAccelerator(KeyStroke.getKeyStroke('X', InputEvent.CTRL_DOWN_MASK));
        exitItem.setToolTipText("Exit application");
        exitItem.addActionListener(exitListener);

        file.add(openItem);
        file.addSeparator();
        file.add(exitItem);
        return file;
    }

    private JMenu createToolsMenu() {
        JMenu tools = new JMenu("Tools");
        tools.setMnemonic(KeyEvent.VK_T);

        JMenuItem consoleItem = new JMenuItem("Console...", KeyEvent.VK_C);
        consoleItem.setAccelerator(KeyStroke.getKeyStroke('S', InputEvent.CTRL_DOWN_MASK));
        consoleItem.setToolTipText("Open console");
        consoleItem.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                jmolPanel.openConsole();
            }
        });

        tools.add(consoleItem);
        return tools;
    }

    private JMenu createHelpMenu() {
        JMenu help = new JMenu("Help");
        help.setMnemonic(KeyEvent.VK_H);

        JMenuItem aboutItem = new JMenuItem("About...", KeyEvent.VK_A);
        aboutItem.setAccelerator(KeyStroke.getKeyStroke('A', InputEvent.CTRL_DOWN_MASK));
        aboutItem.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(getContentPane(),
                        "ChemGUI v1.0\n" +
                                "GUI: Georgy Schukin\n" +
                                "Math: Vladislav Komarov\n" +
                                "Visualization: Jmol",
                        "About", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        help.add(aboutItem);
        return help;
    }

    private void initJmolPanel() {
        jmolPanel = new MyJmolPanel();
        //jmolPanel.setPreferredSize(new Dimension(500, 500));

        getContentPane().add(jmolPanel);
    }

    private void initToolBars() {
        toolbarPanel = new JPanel();
        toolbarPanel.setLayout(new BoxLayout(toolbarPanel, BoxLayout.Y_AXIS));

        fileToolBar = new JToolBar("File Tools", JToolBar.HORIZONTAL);
        isosurfaceToolBar = new JToolBar("Isosurface Tools", JToolBar.HORIZONTAL);;

        initFileToolbar();
        initIsosurfaceToolbar();

        toolbarPanel.add(fileToolBar);
        toolbarPanel.add(isosurfaceToolBar);

        getContentPane().add(toolbarPanel, BorderLayout.NORTH);
    }

    void initFileToolbar() {
        fileToolBar.removeAll();

        JButton openFileButton = new JButton("Open file");
        openFileButton.addActionListener(openFileListener);

        JComboBox ionTypes = new JComboBox(calc.getIonTypes().toArray());
        ionTypes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox)e.getSource();
                currentIonIndex = cb.getSelectedIndex();
            }
        });
        if (ionTypes.getItemCount() > 0) {
            if (currentIonIndex >= ionTypes.getItemCount()) {
                currentIonIndex = ionTypes.getItemCount() - 1;
            }
            ionTypes.setSelectedIndex(currentIonIndex);
        }

        if (currentGridSize < calc.getMinGridSize()) {
            currentGridSize = calc.getMinGridSize();
        }
        JSpinner gridSize = new JSpinner(new SpinnerNumberModel(currentGridSize, calc.getMinGridSize(), 100, 0.1));
        gridSize.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSpinner s = (JSpinner)e.getSource();
                currentGridSize = (Double)s.getValue();
            }
        });
        ((JSpinner.DefaultEditor)gridSize.getEditor()).getTextField().setColumns(4);

        JButton calcButton = new JButton("Generate isosurface");
        calcButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculateBVS();
            }
        });
        if (!calc.isReady()) {
            calcButton.setEnabled(false);
        }

        fileToolBar.add(openFileButton);
        fileToolBar.add(Box.createHorizontalStrut(10));
        fileToolBar.add(new JLabel("Ion type: "));
        fileToolBar.add(ionTypes);
        fileToolBar.add(Box.createHorizontalStrut(10));
        fileToolBar.add(new JLabel("Grid size: "));
        fileToolBar.add(gridSize);
        fileToolBar.add(Box.createHorizontalStrut(10));
        fileToolBar.add(calcButton);

        fileToolBar.setBorder(new EmptyBorder(2, 2, 2, 2));
    }

    void initIsosurfaceToolbar() {
        isosurfaceToolBar.removeAll();

        String cubTypeNames[] = {"BVS", "DA", "GII"};
        JComboBox cubTypes = new JComboBox(cubTypeNames);
        cubTypes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox)e.getSource();
                int newIndex = cb.getSelectedIndex();
                if (currentCubFileIndex != newIndex) {
                    currentCubFileIndex = newIndex;
                    generateIsosurface();
                }
            }
        });
        cubTypes.setSelectedIndex(currentCubFileIndex);

        JSpinner isoCutoff = new JSpinner(new SpinnerNumberModel(1.0, 0.1, 100, 0.1));
        isoCutoff.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSpinner s = (JSpinner)e.getSource();
                double newCutoff = (Double)s.getValue();
                if (currentIsoCutoff != newCutoff) {
                    currentIsoCutoff = newCutoff;
                    generateIsosurface();
                }
            }
        });
        ((JSpinner.DefaultEditor)isoCutoff.getEditor()).getTextField().setColumns(4);

        JButton isoColor = new JButton("   ");
        isoColor.setOpaque(true);
        isoColor.setBackground(currentIsoColor);
        isoColor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color color = JColorChooser.showDialog(null, "Change isosurface color", currentIsoColor);
                if (color != null) {
                    currentIsoColor = color;
                    isoColor.setBackground(currentIsoColor);
                    updateIsosurfaceColor();
                }
            }
        });

        JButton bgColor = new JButton("   ");
        bgColor.setOpaque(true);
        bgColor.setBackground(currentBGColor);
        bgColor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color newColor = JColorChooser.showDialog(null, "Change background color", currentBGColor);
                if (newColor != null) {
                    currentBGColor = newColor;
                    bgColor.setBackground(currentBGColor);
                    updateBackground();
                }
            }
        });

        JSpinner isoTranslucency = new JSpinner(new SpinnerNumberModel(0.0, 0.0, 1.0, 0.1));
        isoTranslucency.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSpinner s = (JSpinner)e.getSource();
                double newTranslucency = (Double)s.getValue();
                if (currentIsoTranslucency != newTranslucency) {
                    currentIsoTranslucency = newTranslucency;
                    updateIsosurfaceTranslucency();
                }
            }
        });
        ((JSpinner.DefaultEditor)isoTranslucency.getEditor()).getTextField().setColumns(3);

        isosurfaceToolBar.add(new JLabel("Cub file: "));
        isosurfaceToolBar.add(cubTypes);
        isosurfaceToolBar.add(Box.createHorizontalStrut(10));
        isosurfaceToolBar.add(new JLabel("Cutoff: "));
        isosurfaceToolBar.add(isoCutoff);
        isosurfaceToolBar.add(Box.createHorizontalStrut(10));
        isosurfaceToolBar.add(new JLabel("Color: "));
        isosurfaceToolBar.add(isoColor);
        isosurfaceToolBar.add(Box.createHorizontalStrut(10));
        isosurfaceToolBar.add(new JLabel("Background: "));
        isosurfaceToolBar.add(bgColor);
        isosurfaceToolBar.add(Box.createHorizontalStrut(10));
        isosurfaceToolBar.add(new JLabel("Translucency: "));
        isosurfaceToolBar.add(isoTranslucency);

        isosurfaceToolBar.setBorder(new EmptyBorder(2, 2, 2, 2));
    }

    private void initStatusBar() {
        statusInfo = new JLabel("");
        statusInfo.setPreferredSize(new Dimension(100, 20));
        getContentPane().add(statusInfo, BorderLayout.SOUTH);
    }

    private void setStatus(String msg) {
        statusInfo.setText(" " + msg);
    }

    private void openFile(File file) {
        try {
            String ext = ChemGUIUtils.getFileExtension(file);
            if (ext.equals("res") || ext.equals("ins")) {
                openShelXFile(file);
            } else if (ext.equals("wxyz")) {
                openWXYZFile(file);
            } else {
                openJmolFile(file);
            }
            setTitle("ChemGUI: " + file.getAbsolutePath());
        }
        catch (FileNotFoundException e) {
            showError(e.getMessage());
        }
    }

    private void checkFileExists(File file) throws FileNotFoundException {
        if (!file.exists()) {
            throw new FileNotFoundException("File not found: " + file.getAbsolutePath());
        }
    }

    private void openShelXFile(File file) throws FileNotFoundException {
        checkFileExists(file);
        try {
            String wxyzFileName = ChemGUIUtils.toWXYZFileName(file);
            String wxyzFilePath = Paths.get(tempDir.getPath().toString(), wxyzFileName).toString();
            ChemRunner.parse(file.getAbsolutePath() + " " + wxyzFilePath);
            openWXYZFile(new File(wxyzFilePath));
        }
        catch (FileNotFoundException e) {
            throw e;
        }
        catch (Exception e) {
            showError(e.getMessage());
        }
    }

    private void openWXYZFile(File file) throws FileNotFoundException {
        checkFileExists(file);
        SwingWorker worker = new SwingWorker<ChemCalc, Void>() {
            @Override
            protected ChemCalc doInBackground() throws Exception {
                setStatus("Opening...");
                return new ChemCalc(file.getAbsolutePath());
            }

            @Override
            public void done() {
                try {
                    calc = get();
                    clearView();
                    initFileToolbar();
                }
                catch (Exception e) {
                    showError(e.getMessage());
                }
                setStatus("Done");
            }
        };
        worker.execute();
    }

    private void openJmolFile(File file) throws FileNotFoundException {
        checkFileExists(file);
        clearView();
        jmolPanel.loadFile(file.getAbsolutePath());
    }

    private void calculateBVS() {
        if (!calc.isReady()) {
            return;
        }
        SwingWorker worker = new SwingWorker<List<String>, Void>() {
            @Override
            protected List<String> doInBackground() throws Exception {
                setStatus("Generating...");
                String tempDirPath = tempDir.getPath().toString();
                return calc.calcCubFiles(currentIonIndex, currentGridSize, tempDirPath);
            }

            @Override
            public void done() {
                try {
                    cubFiles = get();
                    generateIsosurface();
                }
                catch (InterruptedException e) {
                    showError(e.getMessage());
                }
                catch (java.util.concurrent.ExecutionException e) {
                    showError(e.getMessage());
                }
                catch (Exception e) {
                    showError(e.getMessage());
                }
                setStatus("Done");
            }
        };
        worker.execute();
    }

    private void generateIsosurface() {
        clearView();
        updateIsosurface();
        updateIsosurfaceColor();
        updateIsosurfaceTranslucency();
    }

    private void clearView() {
        jmolPanel.script("select all");
        jmolPanel.script("delete");
        jmolPanel.script("isosurface delete");
        updateBackground();
    }

    private void updateIsosurface() {
        if (cubFiles.isEmpty()) {
            return;
        }
        String cubFile = cubFiles.get(currentCubFileIndex).replace("\\", "/");
        jmolPanel.script("isosurface delete");
        jmolPanel.script("isosurface IS0 cutoff " + currentIsoCutoff + " \"" + cubFile + "\" backlit");
    }

    private void updateBackground() {
        jmolPanel.script("background " + ChemGUIUtils.toJmolHexString(currentBGColor));
    }

    private void updateIsosurfaceColor() {
        jmolPanel.script("color isosurface " + ChemGUIUtils.toJmolHexString(currentIsoColor));
    }

    private void updateIsosurfaceTranslucency() {
        jmolPanel.script("isosurface translucent " + currentIsoTranslucency);
    }

    private void showError(String msg) {
        JOptionPane.showMessageDialog(getContentPane(), msg, "Error", JOptionPane.ERROR_MESSAGE);
    }

    /*private ChemGUIWrap getWrapper() {
        if (wrapper == null) {
            wrapper = ChemGUIWrapLoader.getWrapper();
        }
        return wrapper;
    }*/

    public static void main(String[] args) {
        EventQueue.invokeLater( new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                }
                catch (Exception e) {
                    System.out.println(e.toString());
                }
                ChemGUIApp app = new ChemGUIApp();
                app.setVisible(true);
            }
        });
    }
}
