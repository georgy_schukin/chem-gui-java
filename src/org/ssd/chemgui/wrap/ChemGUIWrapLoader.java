package org.ssd.chemgui.wrap;

public class ChemGUIWrapLoader {
    static {
        System.loadLibrary("ChemGUI_wrap");
    }

    public static ChemGUIWrap getWrapper() {
        return new ChemGUIWrap();
    }
}
