package org.ssd.chemgui.ctrl;

import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextFormatter;

public class EditableSpinner<T> extends Spinner<T> {
    public EditableSpinner(SpinnerValueFactory<T> factory) {
        super(factory);
        this.setEditable(true);
        TextFormatter<T> formatter = new TextFormatter<>(factory.getConverter(), factory.getValue());
        this.getEditor().setTextFormatter(formatter);
        factory.valueProperty().bindBidirectional(formatter.valueProperty());
    }
}
